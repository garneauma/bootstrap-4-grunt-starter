/*!
 * SMITH's WordPress+Bootstrap Starter Template Gruntfile
 * http://smith.co
 * Copyright 2016
 *
 * Please refer to README.md for instructions.
 */

// Set to false if on Prod environment
var devEnv = true;


module.exports = function (grunt) {
	require('jit-grunt')(grunt);

	if (devEnv) {
		grunt.initConfig({
			sass: { // Task 
				dist: { // Target 
					options: { // Target options 
						sourceMap: true
					},
					files: { // Dictionary of files 
						'css/style.css': 'scss/style.scss'
					}
				}
			},
			concat: {
				options: {
					separator: ';'
				},
				dist: {
					src: ['js/in-house/*.js'],
					dest: 'js/main.js'
				}
			},
			watch: {
				styles: {
					files: ['scss/**/*.scss'], // which files to watch
					tasks: ['sass'],
					options: {
						nospawn: true
					}
				}
			}
		});
		grunt.registerTask('default', ['sass', 'concat', 'watch']);
	} else {
		grunt.initConfig({
			sass: { // Task 
				dist: { // Target 
					options: { // Target options 
						sourceMap: true
					},
					files: { // Dictionary of files 
						'css/style.css': 'scss/style.scss'
					}
				}
			},
			concat: {
				options: {
					separator: ';'
				},
				dist: {
					src: ['js/in-house/*.js'],
					dest: 'js/main.js'
				}
			},
			uglify: {
				dist: {
					files: {
						'js/main.min.js': ['js/main.js']
					}
				}
			},
			cssmin: {
				options: {
					shorthandCompacting: false,
					roundingPrecision: -1
				},
				target: {
					files: {
						'css/style.min.css': ['css/style.css']
					}
				}
			}
		});
		grunt.registerTask('default', ['sass', 'concat', 'uglify', 'cssmin']);
	}
};