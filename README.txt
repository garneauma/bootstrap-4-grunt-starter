Instructions to create a new project using Bootstrap and SASS. Compiled through Grunt.

-- Prerequisites --
You have to have Ruby and Sass install in order for the compiler to work.
Once you have Ruby installed, run this command to install Sass: gem install sass

1. Open command prompt using administrator mode
2. Go to the directory of this project
3. run: npm install -g
4. run: grunt